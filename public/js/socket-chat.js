var socket = io();

var params = new URLSearchParams(window.location.search);

if (!params.has('name') || !params.has('room')) {
    window.location = 'index.html';
    throw new Error('El nombre y sala son necesarios');
}

var app = new Vue({
    el: '#main-wrapper',
    data: {
        user: {
            name: params.get('name'),
            room: params.get('room')
        },
        people: [],
        newMessage: '',
        messages: [],
        search: ''
    },
    mounted: function() {
        let _this = this;

        // Escuchar cuando el usuario se conecta.
        socket.on('connect', function() {

            console.log('Conectado al servidor');

            socket.emit('enter-chat', _this.user, function(res) {

                _this.people = res.people;

            });

        });

        // Escuchar cuando se desconecta el usuario.
        socket.on('disconnect', function() {

            console.log('Perdimos conexión con el servidor');

        });

        // Escuchar cambios de usuarios cuando un usuario entra o sale del chat.
        socket.on('list-people', function(data) {

            _this.people = data.people;

        });

        // Escuchar mensaje
        socket.on('create-message', function(res) {

            _this.messages.push(res.message);
            scrollToBottom();

        });

        // Escuchar mensaje privado.
        socket.on('private-message', function(message) {

            console.log('Mensaje Privado:', message);

        });


        this.$refs.messageBox.focus();
    },
    filters: {

        formatdate: function(time) {
            let date = new Date(time);
            let hour = `${date.getHours()}:${date.getMinutes()}`;
            return hour;
        }
    },
    computed: {
        filterPeople: function() {
            if (this.search.trim().length > 0) {

                return this.people.filter(person => person.name.toLowerCase().indexOf(this.search) >= 0)

            }

            return this.people;
        }
    },
    methods: {

        onSelectUser(person) {
            window.location = `${window.location.origin}/private-chat.html?name=${params.get('name')}&room=${params.get('room')}&id=${person.id}`
        },

        sendMessage() {
            let _this = this;

            let data = {
                name: this.user.name,
                message: this.newMessage
            }

            // Enviar mensaje
            socket.emit('create-message', data, function(res) {

                if (!res.err) _this.newMessage = '';

                _this.$refs.messageBox.focus();

                _this.messages.push(res.message);

            });
        }
    }
});

function scrollToBottom() {
    let box = document.getElementById('divChatbox');

    if (parseInt(box.offsetHeight + box.scrollTop) == parseInt(box.scrollHeight)) {

        setTimeout(function() {

            box.scrollTo(0, box.scrollHeight);

        }, 300);

    }
}