var socket = io();

var params = new URLSearchParams(window.location.search);

if (!params.has('name') || !params.has('room') || !params.has('id')) {
    window.location = 'index.html';
    throw new Error('El nombre, sala y id son necesarios');
}


var app = new Vue({
    el: '#main-wrapper',
    data: {
        user: {
            name: params.get('name'),
            room: params.get('room')
        },
        newMessage: '',
        messages: []
    },
    mounted: function() {
        let _this = this;

        // Escuchar cuando el usuario se conecta.
        socket.on('connect', function() {

            console.log('Conectado al servidor');

        });

        // Escuchar cuando se desconecta el usuario.
        socket.on('disconnect', function() {

            console.log('Perdimos conexión con el servidor');

        });

        // Escuchar mensaje privado.
        socket.on('private-message', function(message) {

            console.log('Mensaje Privado:', message);

            _this.messages.push(res.message);
            scrollToBottom();

        });


        this.$refs.messageBox.focus();
    },
    filters: {

        formatdate: function(time) {
            let date = new Date(time);
            let hour = `${date.getHours()}:${date.getMinutes()}`;
            return hour;
        }
    },
    methods: {

        sendMessage() {
            let _this = this;

            let data = {
                name: this.user.name,
                message: this.newMessage,
                for: params.get('id')
            }

            // Enviar mensaje
            socket.emit('private-message', data, function(res) {

                if (!res.err) _this.newMessage = '';

                _this.$refs.messageBox.focus();

                _this.messages.push(res.message);

            });
        }
    }
});

function scrollToBottom() {
    let box = document.getElementById('divChatbox');

    if (parseInt(box.offsetHeight + box.scrollTop) == parseInt(box.scrollHeight)) {

        setTimeout(function() {

            box.scrollTo(0, box.scrollHeight);

        }, 300);

    }
}