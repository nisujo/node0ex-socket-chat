const { io } = require('../server');
const { Users } = require('../classes/users');
const { createMessage } = require('../utilities/utilities');
const users = new Users();

io.on('connection', (client) => {

    // Usuario ha ingresado en el chat.
    client.on('enter-chat', (user, callback) => {

        if (!user.name || !user.room) {

            return callback({ err: true, message: 'Nombre/Sala son obligatorios.' });

        }

        client.join(user.room);

        users.addPerson(client.id, user.name, user.room);

        client.broadcast.to(user.room).emit('list-people', { people: users.getPeopleByRoom(user.room) });

        let message = createMessage('Admin', `${user.name} ingresó en el chat.`);
        client.broadcast.to(user.room).emit('create-message', { message });

        callback({ people: users.getPeopleByRoom(user.room) });

    });

    // Usuario ha dejado el chat.
    client.on('disconnect', () => {

        let deletedPerson = users.deletePerson(client.id);
        let message = createMessage('Admin', `${deletedPerson.name} abandonó el chat.`);

        client.broadcast.to(deletedPerson.room).emit('create-message', { message });
        client.broadcast.to(deletedPerson.room).emit('list-people', { people: users.getPeopleByRoom(deletedPerson.room) });

    });


    client.on('create-message', (data, callback) => {

        let person = users.getPerson(client.id);
        let message = createMessage(person.name, data.message);

        client.broadcast.to(person.room).emit('create-message', { message });

        callback({ message });

    });


    // Mensajes privados
    client.on('private-message', (data, callback) => {

        let from = users.getPerson(client.id);
        let to = users.getPerson(data.for);
        let message = createMessage(from.name, data.message);

        if (from.room !== to.room) {
            return callback({ err: true, message: 'Usuarios en diferentes salas.' });
        }

        client.broadcast.to(data.for).emit('private-message', { message });

    });

});