class Users {

    constructor() {
        this.people = [];
    }

    addPerson(id, name, room) {
        let person = { id, name, room };
        this.people.push(person);
        return this.people;
    }

    getPerson(id) {
        let person = this.people.find(person => person.id === id);
        return person;
    }

    getPeople() {
        return this.people;
    }

    getPeopleByRoom(room) {
        let people = this.people.filter(person => person.room === room);
        return people;
    }

    deletePerson(id) {
        let deletedPerson = this.getPerson(id);
        this.people = this.people.filter(person => person.id !== id);
        return deletedPerson;
    }
}


module.exports = { Users }